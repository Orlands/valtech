import { computed, ref } from 'vue'
import countryData from '~/ressources/country.json'
import { defineStore } from 'pinia'

export const useCountryStore = defineStore('country', () => {
  const country = ref([])
  const countryGetters = computed(() => country)
  function fetchCountryData() {
    return countryData

    // Retriving Data from an Api (response would be a json object like countryData)

    //return await new Promise((resolve, reject) => {
    //  axios.get('someUrl')
    //    .then((response) => {
    //      resolve(response.data)
    //    })
    //    .catch(error => reject(error))
    //})
  }

  return { country, countryGetters, fetchCountryData }
})