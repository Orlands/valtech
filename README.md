# Valtech
[![Vue - latest](https://img.shields.io/badge/Vue-latest-4FC08D?logo=vuedotjs&logoColor=4FC08D)](https://vuejs.org/) [![NuxtJS - 3.7.3](https://img.shields.io/badge/NuxtJS-3.7.3-00DC82?logo=nuxtdotjs&logoColor=00DC82)](https://nuxt.com/) [![NodeJs - 18.16.1](https://img.shields.io/badge/NodeJs-18.16.1-339933?logo=nodedotjs&logoColor=339933)](https://nodejs.org/) [![Npm - 9.5.1](https://img.shields.io/badge/Npm-9.5.1-CB3837?logo=npm&logoColor=CB3837)](https://www.npmjs.com/)

You will find below the instruction on how to install and more about the project 

## Assumption

- Code patterns and organization. This will be fundamental while delivering this test.
- Responsive design. This layout must be fluid, so, mobile-first approach is a good way
to start.

- JS Frameworks. You are free to pick whatever framework you feel more comfortable
to work with (VueJs, React, etc), but keep in mind that this choice will be used to
evaluate you regarding design patterns and structure of that specific framework.
- Package Manager. This need to be a solid implementation despite your choice to go:
yarn, npm, pnpm. Review the dependencies used, provide scripts and instructions to
lift the project.
- CSS. We know style libraries might help us in our daily bases, but we don�t think for
this short challenge we need any except grid helpers if you would like.

## Good to know

I choose to operate like i was retrieving the card data and size from an api. 
The user can be deciding which size the card will be display from for example an backOffice
You can also decide to add some more data in the json which will display for some more configurable cards

## Installation
Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev
```

